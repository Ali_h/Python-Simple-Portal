CREATE SCHEMA `pooya` DEFAULT CHARACTER SET utf8 COLLATE utf8_persian_ci ;


CREATE TABLE `pooya`.`student` (
  `name` VARCHAR(15) NOT NULL,
  `family` VARCHAR(45) NOT NULL,
  `stu_id` BIGINT(10) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `average` DOUBLE NOT NULL,
  `pass_lesson` VARCHAR(100) NOT NULL,
  `curent_lesson` VARCHAR(100) NOT NULL,
  `term` INT NOT NULL,
  PRIMARY KEY (`stu_id`),
  UNIQUE INDEX `stu_id_UNIQUE` (`stu_id` ASC));


CREATE TABLE `pooya`.`admin` (
  `name` VARCHAR(15) NOT NULL,
  `family` VARCHAR(20) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
   PRIMARY KEY (`password`));

CREATE TABLE `pooya`.`room` (
  `stu_1` BIGINT(10) NOT NULL,
  `stu_2` BIGINT(10) NOT NULL,
  `stu_3` BIGINT(10) NOT NULL,
  `stu_4` BIGINT(10) NOT NULL,
  `stu_5` BIGINT(10) NOT NULL,
  `score` BIGINT(10) NOT NULL,

CREATE TABLE `pooya`.`loan` (
  `stu_id` BIGINT(12) NOT NULL,
  `score` INT NOT NULL,
  `meghdar` varchar(45) NOT NULL,
  UNIQUE INDEX `stu_id_UNIQUE` (`stu_id` ASC),
  UNIQUE INDEX `score_UNIQUE` (`score` ASC));
