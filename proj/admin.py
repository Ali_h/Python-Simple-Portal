import getpass
import MySQLdb

db = MySQLdb.connect(host="127.0.0.1" , user="root" , passwd="3138" , db='pooya')
cur=db.cursor()

class Admin:

	def __init__(self):		#load kardan tamam data ha az data base va zakhire too list
		self.student=[]		#khandane student az data base va rikhatn dar list
		cur.execute("SELECT * FROM student")
		temp_stu=list(cur.fetchall())
		for i in temp_stu:
			self.student.append(list(i))

		self.loan=[]		#khandane loan az data base va rikhatn dar list
		cur.execute("SELECT * FROM loan")
		temp_loan=list(cur.fetchall())
		for i in temp_loan:
			self.loan.append(list(i))

		self.room=[]		#khandane room az data base va rikhatn dar list
		cur.execute("SELECT * FROM room")
		temp_room=list(cur.fetchall())
		for i in temp_room:
			self.room.append(list(i))

	def add_stu(self):
		stu=[]		#yek list baraye rikhatane daneshjoo ha dar yek list
		name=input('name daneshjoo ra vared konid = ')
		family=input('family daneshjoo ra vared konid = ')
		stu_code=int(input('shomare daneshjooyi ra vared konid = '))
		password=getpass.getpass()
		average=input("moadel daneshjoo ra vared konid = ")
		pass_lesson=input("doroose pass shode ra vared konid = ")
		current_lesson=input("doroose entekhabi ra vared konid = ")
		term=int(input("sale voroodi daneshjoo ra vared konid = "))
		stu=[name,family,stu_code,password,average,pass_lesson,current_lesson,term]
		self.student.append(stu)
		print ("\nAdd Successfully !!")

	def delete_stu(self):
		del_flag=False	#baraye inke befahmim daneshjoo hazf shode ya na
		if len(self.student) > 0:	#inke daneshjooyi mojood ast ya na
			print ("\ndaneshjoo haye mojood be sharhe zir ast : ")
			for index in self.student:
				print ("shomare daneshjooyi : " , index[2])
			id=int(input("\nshomare daneshjooyi ra vared konid = "))
			for i in range(len(self.student)):
				if self.student[i][2] == id:	#search kardan daneshjoo dar list va hazf kardan an
					del self.student[i]
					del_flag=True
					print ("\nDaneshjoo Ba Movafaghiyat Hazf Shod !!")
			if del_flag == False :
				print ("in shomare daneshjooyi mojood nemibashad !!")
		else :
			print("\ndaneshjooyi baraye hazf kardan mojood nist")

	def show_stu(self):
		if len(self.student) > 0 :
			for index in self.student:	#neshan dadane daneshjoo
				print ('name = ' , index[0] , '\t' , 'family = ' , index[1] , '\t' , 'stu_id = ' , index[2] , 'password = ' , index[3] , '\t' , 'average = ' , index[4] , '\t' , 'pass_lesson = ' , index[5] , 'curent_lesson = ' , index[6] , 'term = ' , index[7] , "\n")
		else :
			print ("\ndaneshjooyi baraye Namayesh Mojood nemibashad !!")

	def search_stu(self):
		search_flag=False
		if len(self.student) > 0 :
			id=int(input("\nshomare daneshjooyi ra vared konid = "))
			for index in self.student:
				if index[2] == id:		#search kardan daneshjoo
					search_flag=True
					print ('name = ' , index[0] , '\t' , 'family = ' , index[1] , '\t' , 'stu_id = ' , index[2] , 'password = ' , index[3] , '\t' , 'average = ' , index[4] , '\t' , 'pass_lesson = ' , index[5] , 'curent_lesson = ' , index[6] , 'term = ' , index[7] , "\n")
			if search_flag == False :
				print ("in daneshjoo Mojood nemibashad")
		else:
			print("daneshjooyi baraye Namayesh Mojood nemibashad !!")

	def sort_stu(self):
		print ("\n1)Moratab Sazi Bar Asase Family")
		print ("2)Moratab Sazi Bar Asase Shomare Daneshjooyi")
		index=int(input("Gozine Mored Nazar Ra Vared Konid : "))
		if index == 1:
			cur.execute("SELECT * FROM student order by family")
			c=cur.fetchall()
			index=0
			for index in c:
				print ('name = ' , index[0] , '\t' , 'family = ' , index[1] , '\t' , 'stu_id = ' , index[2] , 'password = ' , index[3] , '\t' , 'average = ' , index[4] , '\t' , 'pass_lesson = ' , index[5] , 'curent_lesson = ' , index[6] , 'term = ' , index[7] , "\n")

		elif index == 2:
			cur.execute("SELECT * FROM student order by stu_id")
			c=cur.fetchall()
			index=0
			for index in c:
				print ('name = ' , index[0] , '\t' , 'family = ' , index[1] , '\t' , 'stu_id = ' , index[2] , 'password = ' , index[3] , '\t' , 'average = ' , index[4] , '\t' , 'pass_lesson = ' , index[5] , 'curent_lesson = ' , index[6] , 'term = ' , index[7] , "\n")

		else :
			print('\nIn Gozine Mojood Nist.')

	def accept_room(self):
		self.accepted_room=[]		#list daneshjooy ha yei ke taeid darkhast shodan
		if len(self.room) == 0:		#naboode darkhaste otagh
			print("\nHich Darkhasti Sabt Nashode ast!")
		elif len(self.room) == 1:	#yek darkhast otagh
			while(1):
				check=input("\nFaghat Darkhaste 1 Otagh Vared Shode Ast. Aya Taeid Mikonid? (Y/N)")
				if check == 'Y' or 'y':
					self.accepted_room.append(self.room[0][0])
					print("\nDarkhaste Taeide Otagh Ba Movafaghiat Sabt Shod!")
					break
				elif check == 'N' or 'n':
					break
				else:
					continue
		else:
			temp_room=[]	#var kardane daneshjoo ha dar in list
			swap=0	#baraye sort kardan bar asase score
			for i in range(len(self.room)):
				temp_room.append(self.room[i])
			num_room=len(temp_room)	#tedade otagh ha
			for i in range(num_room):
				j=i+1
				while j < num_room:	#sort kardan
					if temp_room[i][5] < temp_room[j][5]:
						swap = temp_room[i]
						temp_room[i] = temp_room[j]
						temp_room[j] = swap
					j+=1
			print("\nTedad Otaghe Sabte Shode %d Ta ast." %num_room)
			for i in range(num_room):
				print("\nOtaghe %d : " %(i+1),temp_room[i])
			acc_num=int(input("\nDarkhaste taeide chand Otagh Ra darid : "))
			if acc_num <= num_room:
				for i in range(acc_num):
					print("\nOtagh Baraye In Gorooh Ba Sargoroohe be Shomare Daneshjooyie ( %d ) Taeid Shod." %temp_room[i][0])
					self.accepted_room.append(temp_room[i][0])
			else:
				print("\nTedad Afrad Darkhasti Bishtar Az Tedad Afrade Sabte Nam Karde Mibashad!")

	def save_accept_room_to_db(self):	#save kardane taeid otagh be data base
		for i in range(len(self.accepted_room)):
			cur.execute("DELETE FROM room where stu_1=%s", (self.accepted_room[i], ))
			db.commit()
		print("Save Successfully !!")

	def accept_loan(self):		#baraye taeid kardane vam ha
		self.accepted_loan=[]
		print("\nTedad Afrade Sabte nam Karde %d nafar ast." %len(self.loan))
		z=1
		for i in self.loan:
			print("\nNafar %d) " %z , "\n" , "shomare Daneshjooyi : " , i[0] , "\t" , "emtiyaz : " , i[1] , "\t" , "meghdar vam : " , i[2])
			z += 1
		temp_loan=[]
		swap=0
		for i in range(len(self.loan)):
			temp_loan.append(self.loan[i])
		num_loan=len(temp_loan)
		for i in range(num_loan):
			j=i+1
			while j < num_loan:
				if temp_loan[i][1] > temp_loan[j][1]:
					swap = temp_loan[i]
					temp_loan[i] = temp_loan[j]
					temp_loan[j] = swap
				j+=1
		if len (self.loan) > 0:
			acc_num=int(input("\nDarkhaste taeide chand Nafar Ra darid : "))
			if acc_num <= len(temp_loan):
				for i in range(acc_num):
					print("\nShomare Daneshjooyie ( %d ) Taeid Shod." %temp_loan[i][0])
					self.accepted_loan.append(temp_loan[i][0])
			else:
				print("\nTedad Afrad Darkhasti Bishtar Az Tedad Afrade Sabte Nam Karde Mibashad!")
		else:
			print ("\ndarkhaste vam sabt nashode ast !!")

	def save_accept_loan_to_db(self):		#save kardane taeid vam ha be data base
		for i in range(len(self.accepted_loan)):
			cur.execute("DELETE FROM loan where stu_id=%s", (self.accepted_loan[i], ))
			db.commit()
		print("\nSave Successfully !!")

	def save_daneshjoo_to_db(self):		#save kardane daneshjoo ha be data base
		cur.execute("DELETE FROM student")
		db.commit()
		for index in self.student:
			cur.execute("INSERT into student(name,family,stu_id,password,average,pass_lesson,curent_lesson,term) values (%s,%s,%s,%s,%s,%s,%s,%s)",(index[0],index[1],index[2],index[3],index[4],index[5],index[6],index[7]))
			db.commit()
		print ("\nSave Successfully !!")

admin=Admin()
