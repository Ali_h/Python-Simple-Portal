class capa_temp:
    def __init__(self):
        self.c=0

class node:
    def __init__(self, data=None, capacity=None, _id=None, parent=None, left=None, right=None):
        self.data = data
        self.id = _id
        self.capacity = capacity
        self.parent = parent
        self.right = right
        self.left = left

def add_lesson():   #sakhte derakhte dars ha
    tree=node("memari",20,5)
    temp=node("manteghi",25,4,tree)
    tree.left=temp
    temp=node("nazariyezabanha",20,6,tree)
    tree.right=temp
    tree=tree.left
    temp=node("gosaste",20,3,tree)
    tree.left=temp
    tree=tree.left
    temp=node("riazi1",30,1,tree)
    tree.left=temp
    temp=node("riazi2",30,2,tree)
    tree.right=temp
    tree=tree.parent.parent.right
    temp=node("pishrafte",20,7,tree)
    tree.left=temp
    tree=tree.left
    temp=node("mabani",30,8,tree)
    tree.left=temp
    tree=tree.parent.parent
    return tree

def pishniyaz_check(x,arr,root):    #check kardan pishniyazie
    if root is not None:
        pishniyaz_check(x,arr,root.left)
        pishniyaz_check(x,arr,root.right)
        if x == root.data:
            if root.left is not None:
                arr.append(root.left.data)
            if root.right is not None:
                arr.append(root.right.data)
        return arr

def capacity_check(x,capa,root):    #check kardan zarfiyat
    if root is not None:
        capacity_check(x,capa,root.left)
        capacity_check(x,capa,root.right)
        if root.data == x:
            capa.c=root.capacity
        return capa.c

def capacity_deceres(x,root):       #kaheshe zarfiyat
    if root is not None:
        capacity_deceres(x,root.left)
        capacity_deceres(x,root.right)
        if root.data==x:
            root.capacity=root.capacity-1

def show_tree(root):                #neshan dadane derakhte dar ha
    if root is not None:
        show_tree(root.left)
        show_tree(root.right)
        show_node(root)

def show_node(node):
    print(node.data, "(", node.id, ") :", node.capacity)

sample=add_lesson()
capacity = capa_temp()
