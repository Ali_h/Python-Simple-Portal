INF = 999999999 #baraye in ke betavanim binahayat ro besazim
def floyd_Warshall(graph):
    numberOfNodes = len(graph)
    dist = graph
    for k in range(numberOfNodes):
        for i in range(numberOfNodes):
            for j in range(numberOfNodes):
                dist[i][j] = min(dist[i][j] ,dist[i][k]+ dist[k][j])
    for i in range(numberOfNodes):
        for j in range(numberOfNodes):
            if(dist[i][j] == INF):
                print ("INF\t",end="")
            else:
                print (str(dist[i][j]) + "\t",end="")
            if j == numberOfNodes-1:
                print ("\n")
