import MySQLdb
from tree import *

db = MySQLdb.connect(host="127.0.0.1" , user="root" , passwd="3138" , db='pooya')
cur=db.cursor()

class Daneshjoo:

	def __init__(self):		#load kardan tamam data ha az data base va zakhire too list
		self.stu_room=[]	#baraye khandan data base room va rikhtan an dar yek list
		cur.execute("SELECT * FROM room")
		temp_room=list(cur.fetchall())
		for i in temp_room:
			self.stu_room.append(list(i))

		self.stu_loan=[]	#baraye khandan data base loan va rikhtan an dar yek list
		cur.execute("SELECT * FROM loan")
		temp_loan=list(cur.fetchall())
		for i in temp_loan:
			self.stu_loan.append(list(i))

		self.stu_information=[]		#baraye khandan data base student va rikhtan an dar yek list
		cur.execute("SELECT * FROM student")
		temp_info=list(cur.fetchall())
		for i in temp_info:
			self.stu_information.append(list(i))

	def get_room(self):
		stu_check=[]	#listi barye inke check konad yek daneshjoo do bar otagh nagirad
		z=0
		for i in self.stu_room:
			for j in range(5):
				if i[j] != 0:
					stu_check.append(int(self.stu_room[z][j]))	#inja stu_check ra por mikonim
			z+=1

		stu_score=0	#hesab kardane emtiyaz
		stu_num=[]	#shomare daneshjooyi ha ra ke darkhaste otagh midahand ra dar in list zakhire mikonad
		year_temp=0	#emtiyazi ast ke be otagh dade mishavad bar asase sale voroodi
		index=4		#chon 4 ta ozv digar mikhahim az 4 shoroo mikonim ta be 0 beresim
		id_temp=int(input("shomare daneshjooyi khod ra vared konid : "))	#gereftan yek shomare daneshjooyi
		if id_temp not in stu_check:	#barresi kardan tekrari naboodan shomare daneshjooyi
			year_temp=int(input("sale voroodi khod ra vared konid : "))	#gereftan sale voroodi
			stu_num.append(id_temp) #por kardan list shomare daneshjooyi
			stu_check.append(id_temp)
			stu_score=1000 + year_temp	#dadane emtiyaz be har otagh
			print("\nshoma be onvane sar goroohe otagh entekhab shodid .\nname 4 ozv digar ra vared konid(agar kamtar az 5 nafar hastid baghiye ozv ha ra 0 vared koind.)")
			while index>0:	#gereftan 4 ozv digar
				id_temp=int(input("shomare daneshjooyi khod ra vared konid : "))
				if id_temp != 0:	#vared kardan daneshjoo ha va mohasebe emtiyaz anha
					if id_temp not in stu_check:
						year_temp=int(input("sale voroodi khod ra vared konid : "))
						stu_num.append(id_temp)
						stu_score += (1000 + year_temp)
						index-=1
					else:
						print("shoma ghablan darkhaste otagh dadeid!")
				else:
					stu_num.append(id_temp)
					index-=1

		else:
			print("shoma ghablan darkhaste otagh dadeid!")
		if stu_score != 0:
			stu_num.append(stu_score)
			self.stu_room.append(stu_num)

	def save_room_to_db(self):	#baraye save kardane darkhast otagh be data base
		cur.execute("DELETE FROM room")
		db.commit()
		for j in self.stu_room:
			cur.execute("INSERT into room(stu_1,stu_2,stu_3,stu_4,stu_5,score) values (%s,%s,%s,%s,%s,%s)",(j[0],j[1],j[2],j[3],j[4],j[5]))
			db.commit()
		print ("save to data base successfully!!")

	def get_loan(self):
		stu_check=[]	#baraye inke yek nafar do bar vam nagirad
		meghdar_vam=""
		z=0
		for i in self.stu_loan:		#vared kardane daneshjoo ha dar in list
			stu_check.append(int(self.stu_loan[z][0]))
			z+=1
		temp_score=0
		if z != 0: #agar hade aghal yek nafar too data base bood
			temp_score=int(self.stu_loan[z-1][1]) 	#baraye inke be nafar badi ke mikhad darkhast bede yek emtiya bishtar az nafar ghable bede
		temp_id=int(input("\nshomare daneshjooyi khod ra vared konid : "))
		if temp_id not in stu_check:	#hesab kardan score har nafar
			while(1):
				meghdar=int(input("\ndarkhast che meghdar Vam ra darid ? (300,600) "))
				if meghdar == 300 :		#darkhast vam 300 tomani
					meghdar_vam += str(meghdar)
					temp_score+=1
					break
				elif meghdar == 600 :		#darkhast vam 600 tomani
					meghdar_vam += str(meghdar)
					temp_score+=1
					break
				else:
					print ("\nin meghdar vam mojaz nemibashad !!")
					continue
			print("\nDarkhast Vam e Shoma Ba Movafaghiat Sabt Shod !!")
		else:
			print("\nshoma ghablan darkhaste vam dadeid!")
		if temp_score != 0:
			loan_temp=[]
			loan_temp.append(temp_id)
			loan_temp.append(temp_score)
			loan_temp.append(meghdar_vam)
			self.stu_loan.append(loan_temp)

		meghdar_vam=""		#khali kardane reshte baraye mohasebe dobare meghdar_vam

	def save_loan_to_db(self):	#baraye save kardane darkhase vam be data base
		cur.execute("DELETE FROM loan")
		db.commit()
		for j in self.stu_loan:
			cur.execute("INSERT into loan(stu_id,score,meghdar) values (%s,%s,%s)",(j[0],j[1],j[2]))
			db.commit()
		print ("\nsave to data base successfully!!")

	def entekhab_vahed(self, lesson_tree):
		self.selected_lessons = ""		#reshte khali baraye rikhtan dars ie ke mikhahim entekhab konim
		rezerv=[]		#baraye rezerv ha
		pish=[]			#baraye pishniaz ha
		current=""		#baraye Darshaye entekhab karde
		capa=0			#zarfiyat
		pas=""			#Darshaye pass shode
		self.temp_id = input("Shomare Daneshjooyie Khod ra vared Konid : ")
		for i in self.stu_information:
			if int(self.temp_id) in list(i):
				pas += i[5]
				print("\nDorose Pass Shode Shoma Barabar Ast ba : ",i[5])

		for i in self.stu_information:
			if int(self.temp_id) in list(i):
				current += i[6]
				if current != 0:
					print("\nDorose entekhabi in terme shoma Barabar Ast ba : ",i[6])

		passed = pas.split(",")
		print("\nDarshaye Mojood Baraye Entekhab Be Sharhe Zir Mibashad : ")
		show_tree(lesson_tree)
		dars=input("\nname dars ra vared konid = ")
		if dars in passed:
			print ("shoma in dars ra ghablan pas kardeid!!")
		else:
			pish=pishniyaz_check(dars,pish,lesson_tree)
			i=0
			tedad_tekrar=0
			while(i < len(pish)):
				if pish[i] in passed :
					tedad_tekrar += 1
				i += 1
			if tedad_tekrar == len(pish):
				capa=capacity_check(dars,capacity,lesson_tree)
				if capa != 0:
					if len(current) > 1 :	#agar Darshaye entekhab shode bish az 1 bashad
						current = current + "," + dars
						self.selected_lessons += current
						capacity_deceres(dars,lesson_tree)
					else :
						self.selected_lessons += dars
						capacity_deceres(dars,lesson_tree)
				else :
					print("zarfiyat in dars kamel shode ast.\nin dars baraye shoma rezarv shod")
					rezarv.append(temp_id)
			else :
				print ("shoma pishniaz haye in dars ra pass nakardid")

	def hazfe_vahed(self):
		self.hazf_vahed=""
		curent_les=""
		self.temp_id = input("Shomare Daneshjooyie Khod ra vared Konid : ")
		for i in self.stu_information:
			if int(self.temp_id) in list(i):
				curent_les += i[6]
				print("\nDorose feli Shode Shoma Barabar Ast ba : ",i[6])

		list_curent_les = curent_les.split(",")

		hazf=input("\nkodam dars ra mikhahid hazf konid ? ")
		if hazf in list_curent_les:
			if len(list_curent_les) > 1 :
				list_curent_les.remove(hazf)
				str_list_curent=''.join(list_curent_les)
				self.hazf_vahed += str_list_curent
			print ("\ndarkhase shoma baraye hazf sabt shod")
		else:
			print("\nshoma in dars ra nadarid.")

	def save_entekhabvahed_to_db(self):		#save kardane entekhab vahed dar data base
		for i in self.stu_information:
			cur.execute("UPDATE student set curent_lesson='"+self.selected_lessons+"' where stu_id='"+str(self.temp_id)+"'")
			db.commit()
		print ("\nsave to data base successfully!!")

	def save_hazf_to_db(self):		#save kardane hazf vahed ha too data base
		for i in self.stu_information:
			cur.execute("UPDATE student set curent_lesson='"+str(self.hazf_vahed)+"' where stu_id='"+str(self.temp_id)+"'")
			db.commit()
		print ("\nsave to data base successfully!!")

daneshjoo = Daneshjoo()
lessons = add_lesson()
