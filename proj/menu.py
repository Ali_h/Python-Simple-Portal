import getpass		#for get password
import MySQLdb		#for import data base
from warshal import *
from daneshjoo import *
from admin import *

db = MySQLdb.connect(host="127.0.0.1" , user="root" , passwd="3138" , db='pooya')
cur=db.cursor()

def call_student_from_db():		#load kardane data haye student dar list
	student=[]		#khandane student az data base va rikhatn dar list
	cur.execute("SELECT * FROM student")
	temp_stu=list(cur.fetchall())
	for i in temp_stu:
		student.append(list(i))
	return student

def main_menu(stu):	#barname az inja shoroo mishavad ke menu asli ast
	while (1):
		print("\n1) Amoozeshi")
		print("2) Refahi")
		print("3) Warshal Floyed")
		print("0) Exit")
		index=int(input("Gozine Mored Nazar Ra Vared Konid : "))
		if index == 1 :
			amoozeshi_menu(stu)
			break
		elif index == 2 :
			exist_flag = False
			if len(stu) > 0:
				temp_id=int(input('\nshomare daneshjooyi khod ra vared konid = '))
				for index in stu:
					if temp_id in index:
						exist_flag = True
						stu_pass=getpass.getpass()
						if stu_pass in index:
							print ('\npassword accept !')
							refahi_menu(stu)
							break
						else:
							print('\npassword NOT accept !')
							continue
				if exist_flag == False:
					print('\nshomare daneshjooyi mojod nist !')

			else:
				print("\nHanooz Dar In Bakhsh Daneshjooyi Vared Nashoda Ast . Be Admin Moraje e Konid .")

		elif index == 3:
			print()
			graph = [[0,5,INF,10],[INF,0,3,INF],[INF, INF,0,1],[INF, INF, INF,0]]
			floyd_Warshall(graph)
			continue

		elif index == 0:
			break

		else :
			print("Gozine Ra Eshteba Vared Kardid . Dobare Talash Konid !\n")
			continue

def amoozeshi_menu(stu):	#baraye raftan be menu Amoozeshi
	while(1):
		print("\n1) Daneshjooyi")
		print("2) Modiriyati")
		print("0) Bazgasht")
		index=int(input("Gozine Mored Nazar Ra Vared Konid : "))
		if index == 1 :
			exist_flag = False
			if len(stu) > 0:
				temp_id=int(input('\nshomare daneshjooyi khod ra vared konid = '))
				for index in stu:
					if temp_id in index:
						exist_flag = True
						stu_pass=getpass.getpass()
						if stu_pass in index:
							print ('\npassword accept !')
							daneshjoo_menu(stu)
							break
						else:
							print('\npassword NOT accept !')
							continue
				if exist_flag == False:
					print('\nshomare daneshjooyi mojod nist !')

			else:
				print("\nHanooz Dar In Bakhsh Daneshjooyi Vared Nashoda Ast . Be Admin Moraje e Konid .")

		elif index == 2 :
			print ("\nbaraye vared shodan be bakhsh modiriyat PASSWORD admin ra vared konid.")
			psw=getpass.getpass()
			cur.execute("SELECT COUNT(password) FROM admin where password =%s",(psw,))
			a=cur.fetchall()
			if a[0][0]==1:
				print ("\npassword accepted!")
				modiriyat_menu(stu)	#raftan be menu Modiriyati
				break
			else:
				print ()
				print('\npassword NOT accept !')
				break

		elif index == 0 :
			main_menu(stu)	#baraye bazghash be menu asli
			break

		else :
			print("Gozine Ra Eshteba Vared Kardid . Dobare Talash Konid !")
			continue

def refahi_menu(stu):	#baraye raftan be menu refahi
	while(1):
		print("\n1) Darkhast Vam")
		print("2) Darkhaste Otagh")
		print("3) Save Room To DataBase")
		print("4) Save loan To DataBase")
		print("0)Bazgasht")
		choose=int(input("Gozine Mored Nazar Ra Vared Konid : "))
		if choose == 1:
			daneshjoo.get_loan()
			continue
		elif choose == 2:
			daneshjoo.get_room()
			continue
		elif choose == 3 :
			daneshjoo.save_room_to_db()
			continue
		elif choose == 4 :
			daneshjoo.save_loan_to_db()
			continue
		elif choose == 0:
			main_menu(stu)
			break
		else :
			print("Gozine Ra Eshteba Vared Kardid . Dobare Talash Konid !\n")
			continue

def daneshjoo_menu(stu):	#raftan be menu daneshjooyi
	while(1):
		print("\n1) Entekhab Vahed")
		print("2) Hazf O Ezafe")
		print("3) Save Entekhab Vahed To Database")
		print("4) Save Hazf Vahed To Database")
		print("0) Bazghash")
		choose=int(input("Gozine Mored Nazar Ra Vared Konid : "))
		if choose == 1:
			daneshjoo.entekhab_vahed(lessons)
			continue
		elif choose == 2:
			daneshjoo.hazfe_vahed()
			continue
		elif choose ==3 :
			daneshjoo.save_entekhabvahed_to_db()
			continue
		elif choose == 4 :
			daneshjoo.save_hazf_to_db()
			continue
		elif choose == 0:
			amoozeshi_menu(stu)
			continue
		else :
			print("Gozine Ra Eshteba Vared Kardid . Dobare Talash Konid !\n")
			continue

def modiriyat_menu(stu):	#raftan be menu Modiriyati
	while(1):
		print("\n1) Anjam Kar haye Daneshjooyie")
		print("2) Taeide Vam Daneshjooyi")
		print("3) Taeide Otagh")
		print("4) Save Loan To DataBase")
		print("5) Save Room To DataBase")
		print("0) Bazgasht")
		choose=int(input("Gozine Mored Nazar Ra Vared Konid : "))
		if choose == 1:
			while(1):
				print("\n1) Afzoodan Daneshjoo")
				print("2) Hazf Daneshjoo")
				print("3) Namayesh Daneshjoo")
				print("4) Jostejoo Daneshjoo")
				print("5) Moratab Sazi Daneshjoo")
				print("6) Save Daneshjoo To DataBase")
				print("0) Bazgasht")
				check=int(input("Gozine Mored Nazar Ra Vared Konid : "))
				if check == 1:
					admin.add_stu()
					continue
				elif check == 2:
					admin.delete_stu()
					continue
				elif check == 3:
					admin.show_stu()
					continue
				elif check == 4:
					admin.search_stu()
					continue
				elif check == 5:
					admin.sort_stu()
					continue
				elif check == 6:
					admin.save_daneshjoo_to_db()
					continue
				elif check == 0:
					modiriyat_menu(stu)
					continue
				else:
					 print("\nGozine Ra Eshteba Vared Kardid . Dobare Talash Konid !")
					 continue

		elif choose == 2:
			admin.accept_loan()
			continue

		elif choose == 3:
			admin.accept_room()
			continue

		elif choose == 4:
			admin.save_accept_loan_to_db()
			continue

		elif choose == 5:
			admin.save_accept_room_to_db()
			continue

		elif choose == 0 :
			main_menu(stu)
			break

		else :
			print("Gozine Ra Eshteba Vared Kardid . Dobare Talash Konid !\n")
			continue
